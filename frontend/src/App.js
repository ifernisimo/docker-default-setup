import React from "react";
import logo from "./logo.svg";
import "./App.css";
import axios from "axios";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> Here we go
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <button
        type="button"
        onClick={() => {
          axios("/api/testwithcurrentuser").then((res) => console.info(res));
        }}
      >
        Make reques
      </button>
    </div>
  );
}

export default App;
